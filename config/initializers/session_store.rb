# Be sure to restart your server when you modify this file.

Rails.application.config.session_store :cookie_store, key: '_sample_app_session'

if Rails.env.test?
  CarrierWave.configure do |config|
    config.enable_processing = false
  end
end
